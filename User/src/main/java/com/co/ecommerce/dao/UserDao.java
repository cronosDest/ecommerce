package com.co.ecommerce.dao;

import com.co.ecommerce.model.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;


@Service
@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {SystemException.class})
public class UserDao implements IUserDao {

    @PersistenceContext
    private EntityManager em;

    /**
     * Method used to find the email and password
     *
     * @param user user login request
     * @return user login
     * @throws NotAuthorizedException it can happen if the data base can't find the user
     * @author Orlando Velasquez
     */
    @Override
    public User findUserByEmailAndPassword(User user) throws NotAuthorizedException {
        try {
            return em.createNamedQuery("User.findByEmailAndPassword", User.class)
                    .setParameter("email", user.getEmail())
                    .setParameter("password", user.getPassword())
                    .getSingleResult();
        } catch (NoResultException ex) {
            throw new NotFoundException("Not found");
        } catch (Exception ex) {
            throw ex;
        }
    }
}
