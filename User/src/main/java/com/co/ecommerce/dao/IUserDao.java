package com.co.ecommerce.dao;

import com.co.ecommerce.model.User;

import javax.ws.rs.NotAuthorizedException;

public interface IUserDao {

    User findUserByEmailAndPassword(User user) throws NotAuthorizedException;

}
