package com.co.ecommerce.restController;

import com.co.ecommerce.domain.UserDto;
import com.co.ecommerce.manager.IUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping(value = "/users")
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class UserRestController implements IUserRestController {

    private final IUserManager iUserManager;

    @Autowired
    public UserRestController(IUserManager iUserManager) {
        this.iUserManager = iUserManager;
    }

    /**
     * Method used to login
     *
     * @param body request object
     * @return ResponseEntity an object response
     * @author Orlando Velasquez
     */
    @PostMapping("/login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseEntity login(@RequestBody UserDto body) {
        try {
            iUserManager.login(body);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (NotAuthorizedException ex) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex);
        }
    }

}