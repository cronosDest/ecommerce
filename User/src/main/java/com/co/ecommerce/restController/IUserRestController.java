package com.co.ecommerce.restController;

import com.co.ecommerce.domain.UserDto;
import org.springframework.http.ResponseEntity;

public interface IUserRestController {

    /**
     * @author Orlando Velasquez
     * @param body request object
     * @return ResponseEntity an object response
     */
    ResponseEntity login(UserDto body);

}
