package com.co.ecommerce.utils.converter;

import com.co.ecommerce.domain.UserDto;
import com.co.ecommerce.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    /**
     * @param userDto user domain
     * @return user database entity
     */
    public User convertUserDtoToUser(UserDto userDto){
        return User.builder()
                .email(userDto.getEmail())
                .password(userDto.getPassword())
                .isAdmin(userDto.isAdmin())
                .build();
    }

    /**
     * @param user user database entity
     * @return user domain
     */
    public UserDto convertUserToUserDto(User user){
        return UserDto.builder()
                .email(user.getEmail())
                .password(user.getPassword())
                .isAdmin(user.getAdmin())
                .build();
    }

}
