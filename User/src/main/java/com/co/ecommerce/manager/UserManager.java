package com.co.ecommerce.manager;

import com.co.ecommerce.domain.UserDto;
import com.co.ecommerce.utils.converter.UserConverter;
import com.co.ecommerce.dao.IUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;

@Service
public class UserManager implements IUserManager {

    private final IUserDao iUserDao;
    private final UserConverter userConverter;

    @Autowired
    public UserManager(IUserDao iUserDao, UserConverter userConverter) {
        this.iUserDao = iUserDao;
        this.userConverter = userConverter;
    }

    /**
     * Method used convert domain entity to database entity
     *
     * @param userDto object to login
     * @return user login
     * @throws NotAuthorizedException it can happen if the data base can't find the user
     * @author Orlando Velasquez
     */
    @Override
    public UserDto login(UserDto userDto) throws NotAuthorizedException {
        try {
            return userConverter.convertUserToUserDto(
                    iUserDao.findUserByEmailAndPassword(
                            userConverter.convertUserDtoToUser(userDto)));
        } catch (NotFoundException ex) {
            throw new NotAuthorizedException("Usuario o contraseña incorrecto");
        } catch (Exception ex) {
            throw ex;
        }
    }

}
