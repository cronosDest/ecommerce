package com.co.ecommerce.manager;

import com.co.ecommerce.domain.UserDto;

import javax.ws.rs.NotAuthorizedException;

public interface IUserManager {

    UserDto login(UserDto userDto) throws NotAuthorizedException;

}
