package com.co.ecommerce.domain;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class UserDto {
    private String email;
    private String password;
    private boolean isAdmin;
}

