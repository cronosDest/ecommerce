package com.co.ecommerce.domain;

import lombok.*;

import java.io.Serializable;
import java.util.HashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CurrencyResponseDto implements Serializable {

    private String source;
    private HashMap<String,Double> quotes;

}
