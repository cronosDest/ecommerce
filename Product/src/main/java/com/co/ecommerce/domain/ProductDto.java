package com.co.ecommerce.domain;

import lombok.*;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ProductDto {
    private String name;
    private String description;
    private double weight;
    private double usdPrice;
    private double copPrice;
    private long category;
    private String firstPicture;
    private String secondPicture;
    private String thirdPicture;
}

