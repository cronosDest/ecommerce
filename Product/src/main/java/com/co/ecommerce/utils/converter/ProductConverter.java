package com.co.ecommerce.utils.converter;

import com.co.ecommerce.domain.ProductDto;
import com.co.ecommerce.model.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductConverter {

    /**
     * Method used to convert product domain entity to product database entity
     *
     * @param productDto product domain entity
     * @return product database entity
     */
    public Product convertProductDtoToProduct(ProductDto productDto) {
        return Product.builder()
                .name(productDto.getName())
                .description(productDto.getDescription())
                .weight(productDto.getWeight())
                .usdPrice(productDto.getUsdPrice())
                .category(productDto.getCategory())
                .firstPicture(productDto.getFirstPicture())
                .secondPicture(productDto.getSecondPicture())
                .thirdPicture(productDto.getThirdPicture())
                .build();
    }

    /**
     * Method used to convert product database entity to product domain entity
     *
     * @param product product database entity
     * @return product domain entity
     */
    public ProductDto convertProductToProductDto(Product product) {
        return ProductDto.builder()
                .name(product.getName())
                .description(product.getDescription())
                .weight(product.getWeight())
                .usdPrice(product.getUsdPrice())
                .category(product.getCategory())
                .firstPicture(product.getFirstPicture())
                .secondPicture(product.getSecondPicture())
                .thirdPicture(product.getThirdPicture())
                .build();
    }

}
