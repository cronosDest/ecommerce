package com.co.ecommerce.utils.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EnvironmentVariables {
    @Value("${cloud.aws.credentials.accessKey}")
    public String ACCESS_KEY;

    @Value("${cloud.aws.credentials.secretKey}")
    public String SECRET_KEY;

    @Value("${app.awsServices.bucketName}")
    public String BUCKET;

    @Value("${currency.api.path}")
    public String CURRENCY_API_PATH;

}
