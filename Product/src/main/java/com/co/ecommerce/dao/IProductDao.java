package com.co.ecommerce.dao;

import com.co.ecommerce.model.Product;

import javax.ws.rs.NotFoundException;
import java.util.List;

public interface IProductDao {

    void create(Product product);

    void update(Product product);

    Product findProductById(long id) throws NotFoundException;

    List<Product> find(Integer page, Integer pageSize, Integer categoryId);
}
