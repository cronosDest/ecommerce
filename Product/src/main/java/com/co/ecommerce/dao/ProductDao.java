package com.co.ecommerce.dao;

import com.co.ecommerce.model.Product;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.NotFoundException;
import java.util.List;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class})
public class ProductDao implements IProductDao {

    @PersistenceContext
    private EntityManager em;

    /**
     * Method used to create a product
     *
     * @param Product product database entity
     */
    @Override
    public void create(Product Product) {
        em.persist(Product);
        em.flush();
    }

    /**
     * Method used to update a product
     *
     * @param Product product database entity
     */
    public void update(Product Product) {
        em.merge(Product);
        em.flush();
    }

    /**
     * Method used to find a product by id
     *
     * @param id product identifier
     * @return product database entity
     * @throws NotFoundException validate if the product doesn't exist
     */
    public Product findProductById(long id) throws NotFoundException {
        try {
            return em.createNamedQuery("Product.findById", Product.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (NoResultException ex) {
            throw new NotFoundException("Not found");
        }
    }

    /**
     * Method used to find products
     *
     * @param page       page number
     * @param pageSize   page size number
     * @param categoryId category identifier
     * @return return product list
     */
    @Override
    public List<Product> find(Integer page, Integer pageSize, Integer categoryId) {
        StringBuilder newQuery = new StringBuilder();
        newQuery.append("SELECT NEW Product (p.productId,p.description,p.weight,p.usdPrice,p.category," +
                " p.firstPicture,p.secondPicture,p.thirdPicture,p.name) from Product p");
        if (categoryId != null) {
            newQuery.append(" where p.category=").append(categoryId);
        }
        Query query = em.createQuery(newQuery.toString());

        query.setFirstResult((page - 1) * pageSize);
        query.setMaxResults(pageSize);
        return query.getResultList();
    }

}
