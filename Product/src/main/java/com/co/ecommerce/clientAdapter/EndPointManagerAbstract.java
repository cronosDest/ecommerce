package com.co.ecommerce.clientAdapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public abstract class EndPointManagerAbstract {

    private EndPointConfig endpointConfig;

    @Autowired
    public EndPointManagerAbstract(EndPointConfig endpointConfig) {
        this.endpointConfig = endpointConfig;
    }


    public ResponseEntity endpointConsumerClient(final String pathEndpoint,
                                                 final Class<?> typeResponse,
                                                 final HttpMethod method) {

        RestTemplate clientConsumer = new RestTemplate();
        HttpHeaders httpHeadersConsumer = endpointConfig.createAuthenticationHeaders();
        return clientConsumer.exchange(pathEndpoint, method, new HttpEntity<>(httpHeadersConsumer), typeResponse);
    }

}
