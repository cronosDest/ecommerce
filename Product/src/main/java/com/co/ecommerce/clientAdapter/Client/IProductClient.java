package com.co.ecommerce.clientAdapter.Client;

import com.co.ecommerce.domain.CurrencyResponseDto;

public interface IProductClient {

    CurrencyResponseDto findCOPCurrency();

}
