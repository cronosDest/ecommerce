package com.co.ecommerce.clientAdapter.Client;

import com.co.ecommerce.clientAdapter.EndPointConfig;
import com.co.ecommerce.clientAdapter.EndPointManagerAbstract;
import com.co.ecommerce.domain.CurrencyResponseDto;
import com.co.ecommerce.utils.constant.EnvironmentVariables;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ProductClient extends EndPointManagerAbstract implements IProductClient {

    private final EnvironmentVariables environmentVariables;

    public ProductClient(EndPointConfig endpointConfig, EnvironmentVariables environmentVariables) {
        super(endpointConfig);
        this.environmentVariables = environmentVariables;
    }

    @Override
    public CurrencyResponseDto findCOPCurrency() {
        try {
            ResponseEntity<CurrencyResponseDto> response = endpointConsumerClient(
                    environmentVariables.CURRENCY_API_PATH, CurrencyResponseDto.class, HttpMethod.GET);
            return response.getBody();
        } catch (Exception ex) {
            throw ex;
        }
    }
}
