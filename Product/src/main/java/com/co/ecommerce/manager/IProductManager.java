package com.co.ecommerce.manager;

import com.co.ecommerce.domain.ProductDto;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.NotFoundException;
import java.io.IOException;
import java.util.List;

public interface IProductManager {

    void create(ProductDto productDto);

    ProductDto findById(long productId) throws NotFoundException;

    void uploadImages(MultipartFile[] files, long productId) throws NotFoundException, IOException;

    List<ProductDto> find(Integer page, Integer sizePage, Integer categoryId);

}
