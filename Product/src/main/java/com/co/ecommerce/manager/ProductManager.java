package com.co.ecommerce.manager;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.iotdata.model.ConflictException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.co.ecommerce.clientAdapter.Client.IProductClient;
import com.co.ecommerce.domain.CurrencyResponseDto;
import com.co.ecommerce.domain.ProductDto;
import com.co.ecommerce.model.Product;
import com.co.ecommerce.utils.constant.EnvironmentVariables;
import com.co.ecommerce.utils.converter.ProductConverter;
import com.co.ecommerce.dao.IProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.NotFoundException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class ProductManager implements IProductManager {

    private final IProductDao iProductDao;
    private final ProductConverter productConverter;
    public EnvironmentVariables environmentVariables;
    private final IProductClient iProductClient;
    AmazonS3 s3client;

    @Autowired
    public ProductManager(IProductDao iProductDao, ProductConverter productConverter,
                          EnvironmentVariables environmentVariables, IProductClient iProductClient) {
        this.iProductDao = iProductDao;
        this.productConverter = productConverter;
        this.environmentVariables = environmentVariables;
        this.iProductClient = iProductClient;
        //Set AWS crendentials
        AWSCredentials credentials = new BasicAWSCredentials(
                environmentVariables.ACCESS_KEY,
                environmentVariables.SECRET_KEY
        );

        s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_EAST_2)
                .build();
    }

    /**
     * Method used to create a product
     *
     * @param productDto product domain entity
     */
    @Override
    public void create(ProductDto productDto) {
        iProductDao.create(productConverter.convertProductDtoToProduct(productDto));
    }

    /**
     * Method used to find product by id
     *
     * @param productId product identifier
     * @return product domain entity
     * @throws NotFoundException validate ig doesn't exist
     */
    @Override
    public ProductDto findById(long productId) throws NotFoundException {
        try {
            ProductDto productDto = productConverter.convertProductToProductDto(iProductDao.findProductById(productId));
            return calculateCopPrice(productDto);
        } catch (NotFoundException ex) {
            throw new NotFoundException("Not found");
        }
    }

    /**
     * Method used to consume API and calculate COP price
     *
     * @param productDto product domain entity
     * @return product domain entity
     */
    private ProductDto calculateCopPrice(ProductDto productDto) {
        CurrencyResponseDto currencyResponseDto = iProductClient.findCOPCurrency();
        Double copPrice = (currencyResponseDto.getQuotes().get("USDCOP") * productDto.getUsdPrice());
        productDto.setCopPrice(copPrice);
        return productDto;
    }

    /**
     * @param multipartFiles image list to create bulk
     * @param productId      product identifier
     * @throws NotFoundException
     * @throws IOException
     */
    @Override
    public void uploadImages(MultipartFile[] multipartFiles, long productId) throws NotFoundException, IOException {
        Product product = iProductDao.findProductById(productId);
        String[] url = uploadS3ImageBulk(multipartFiles);
        product.setFirstPicture(url[0]);
        product.setSecondPicture(url[1]);
        product.setThirdPicture(url[2]);
        iProductDao.update(product);
    }

    /**
     * Method used to upload image to AWS S3
     *
     * @param multipartFileList images files to create
     * @return product image url list
     * @throws IOException
     */
    private String[] uploadS3ImageBulk(MultipartFile[] multipartFileList) throws IOException {
        String[] urlList = new String[3];
        for (int i = 0; i < multipartFileList.length; i++) {
            MultipartFile multipartFile = multipartFileList[i];

            String fileName = new Date().getTime() + multipartFile.getOriginalFilename();
            File file = convertMultiPartToFile(multipartFile);
            urlList[i] = uploadFileTos3bucket(fileName, file);
            file.delete();
        }
        return urlList;
    }


    /**
     * Method used to convert multipart file to file
     *
     * @param file multipart file to convert
     * @return file
     * @throws IOException Input/Output Exception
     */
    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convertFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
        FileOutputStream fos = new FileOutputStream(convertFile);
        fos.write(file.getBytes());
        fos.close();
        return convertFile;
    }

    /**
     * Method used to send file to AWS S3 client
     *
     * @param fileName file image name
     * @param file     file to upload
     * @return url image
     */
    private String uploadFileTos3bucket(String fileName, File file) {
        s3client.putObject(
                new PutObjectRequest(environmentVariables.BUCKET, fileName, file)
                        .withCannedAcl(CannedAccessControlList.PublicRead));
        URL url = s3client.getUrl(environmentVariables.BUCKET, fileName);
        return url.toString();
    }

    /**
     * Method used to find products
     *
     * @param page       page number
     * @param pageSize   page size number
     * @param categoryId category identifier
     * @return product list
     */
    @Override
    public List<ProductDto> find(Integer page, Integer pageSize, Integer categoryId) {
        if ((page == null || page == 0) || (pageSize == null || pageSize == 0)) {
            throw new ConflictException("The page and the page size can't be empty");
        }
        List<ProductDto> productDtoList = new ArrayList<>();
        for (Product product : iProductDao.find(page, pageSize, categoryId)) {
            productDtoList.add(calculateCopPrice(productConverter.convertProductToProductDto(product)));
        }
        return productDtoList;
    }
}
