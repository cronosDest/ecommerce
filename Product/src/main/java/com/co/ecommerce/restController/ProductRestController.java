package com.co.ecommerce.restController;

import com.amazonaws.services.iotdata.model.ConflictException;
import com.co.ecommerce.domain.ProductDto;
import com.co.ecommerce.domain.ResponseErrorDto;
import com.co.ecommerce.manager.IProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.Consumes;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@RequestMapping(value = "/products")
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class ProductRestController implements IProductRestController {

    private final IProductManager iProductManager;

    @Autowired
    public ProductRestController(IProductManager iProductManager) {
        this.iProductManager = iProductManager;
    }

    /**
     * Method used to create a product
     *
     * @param body product object
     * @return return create response
     */
    @PostMapping("")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseEntity create(@RequestBody ProductDto body) {
        try {
            iProductManager.create(body);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex);
        }
    }

    /**
     * Method use to upload three product images
     *
     * @param productId     product identifier
     * @param firstPicture  first product picture
     * @param secondPicture second product picture
     * @param thirdPicture  third product picture
     * @return product updated
     */
    @PatchMapping("/{productId}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseEntity createUserImage(@PathVariable(value = "productId") long productId,
                                          @RequestParam(value = "firstPicture") MultipartFile firstPicture,
                                          @RequestParam(value = "secondPicture") MultipartFile secondPicture,
                                          @RequestParam(value = "thirdPicture") MultipartFile thirdPicture) {
        try {
            MultipartFile[] files = new MultipartFile[3];
            files[0] = firstPicture;
            files[1] = secondPicture;
            files[2] = thirdPicture;
            iProductManager.uploadImages(files, productId);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex);
        }
    }

    /**
     * Method used to find a product by id
     *
     * @param productId product identifier
     * @return product
     */
    @GetMapping("/{productId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity findProductById(@PathVariable(value = "productId") long productId) {
        try {
            ProductDto productDto = iProductManager.findById(productId);
            return ResponseEntity.status(HttpStatus.OK).body(productDto);
        } catch (NotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex);
        }
    }

    /**
     * Method used to find products
     *
     * @param page page number
     * @param pageSize page size number
     * @param category category identifier
     * @return product list
     */
    @GetMapping("")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity find(@QueryParam("page") Integer page, @QueryParam("pageSize") Integer pageSize,
                               @QueryParam("category") Integer category) {
        try {
            List<ProductDto> productDtoList = iProductManager.find(page, pageSize, category);
            return ResponseEntity.status(HttpStatus.OK).body(productDtoList);
        } catch (ConflictException ex) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseErrorDto(ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex);
        }
    }

}