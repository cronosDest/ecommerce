package com.co.ecommerce.restController;

import com.co.ecommerce.domain.ProductDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.QueryParam;

public interface IProductRestController {
    ResponseEntity create(ProductDto body);

    ResponseEntity createUserImage(long productId, MultipartFile firstImage,
                                   MultipartFile secondImage,
                                   MultipartFile thirdImage);

    public ResponseEntity find(Integer page, Integer pageSize, Integer categoryId);

    public ResponseEntity findProductById(long productId);
}
