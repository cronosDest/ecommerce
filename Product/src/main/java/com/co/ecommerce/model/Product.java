package com.co.ecommerce.model;

import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Builder
@ToString
@NoArgsConstructor
@Table(name = "PRODUCT")
@NamedQueries({
        @NamedQuery(name = "Product.findById", query = "SELECT p FROM Product p WHERE p.productId = :id"),
        @NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p")})
public class Product {
    private long productId;
    private String description;
    private double weight;
    private double usdPrice;
    private long category;
    private String firstPicture;
    private String secondPicture;
    private String thirdPicture;
    private String name;

    @Id
    @SequenceGenerator(
            name = "Product.product_product_id_seq ",
            sequenceName = "\"public\".product_product_id_seq ", allocationSize = 1)
    @GeneratedValue(generator = "Product.product_product_id_seq ")
    @Column(name = "product_id")
    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "weight")
    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Basic
    @Column(name = "usd_price")
    public double getUsdPrice() {
        return usdPrice;
    }

    public void setUsdPrice(double usdPrice) {
        this.usdPrice = usdPrice;
    }

    @Basic
    @Column(name = "category")
    public long getCategory() {
        return category;
    }

    public void setCategory(long category) {
        this.category = category;
    }

    @Basic
    @Column(name = "first_picture")
    public String getFirstPicture() {
        return firstPicture;
    }

    public void setFirstPicture(String firstPicture) {
        this.firstPicture = firstPicture;
    }

    @Basic
    @Column(name = "second_picture")
    public String getSecondPicture() {
        return secondPicture;
    }

    public void setSecondPicture(String secondPicture) {
        this.secondPicture = secondPicture;
    }

    @Basic
    @Column(name = "third_picture")
    public String getThirdPicture() {
        return thirdPicture;
    }

    public void setThirdPicture(String thirdPicture) {
        this.thirdPicture = thirdPicture;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return productId == product.productId &&
                Double.compare(product.weight, weight) == 0 &&
                Double.compare(product.usdPrice, usdPrice) == 0 &&
                category == product.category &&
                Objects.equals(description, product.description) &&
                Objects.equals(firstPicture, product.firstPicture) &&
                Objects.equals(secondPicture, product.secondPicture) &&
                Objects.equals(thirdPicture, product.thirdPicture) &&
                Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, description, weight, usdPrice, category, firstPicture, secondPicture, thirdPicture, name);
    }

    public Product(long productId, String description, double weight, double usdPrice, long category,
                   String firstPicture, String secondPicture, String thirdPicture, String name) {
        this.productId = productId;
        this.description = description;
        this.weight = weight;
        this.usdPrice = usdPrice;
        this.category = category;
        this.firstPicture = firstPicture;
        this.secondPicture = secondPicture;
        this.thirdPicture = thirdPicture;
        this.name = name;
    }


}
