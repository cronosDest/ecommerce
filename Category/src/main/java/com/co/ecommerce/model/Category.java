package com.co.ecommerce.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CATEGORY")
@NamedQueries({
        @NamedQuery(name = "Category.findById", query = "SELECT c FROM Category c WHERE c.categoryId = :id"),
        @NamedQuery(name = "Category.findAll", query = "SELECT c FROM Category c order by c.name asc ")})
public class Category {
    private long categoryId;
    private String name;
    private String picture;
    private Category categoryByPatternCategory;
    private List<Category> categoriesByCategoryId;

    @Id
    @SequenceGenerator(
            name = "Category.category_category_id_seq ",
            sequenceName = "\"public\".category_category_id_seq ", allocationSize = 1)
    @GeneratedValue(generator = "Category.category_category_id_seq ")
    @Column(name = "category_id")
    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "picture")
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return categoryId == category.categoryId &&
                Objects.equals(name, category.name) &&
                Objects.equals(picture, category.picture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryId, name, picture);
    }

    @ManyToOne
    @JoinColumn(name = "pattern_category", referencedColumnName = "category_id")
    public Category getCategoryByPatternCategory() {
        return categoryByPatternCategory;
    }

    public void setCategoryByPatternCategory(Category categoryByPatternCategory) {
        this.categoryByPatternCategory = categoryByPatternCategory;
    }

    @OneToMany(mappedBy = "categoryByPatternCategory")
    public List<Category> getCategoriesByCategoryId() {
        return categoriesByCategoryId;
    }

    public void setCategoriesByCategoryId(List<Category> categoriesByCategoryId) {
        this.categoriesByCategoryId = categoriesByCategoryId;
    }
//
//    public Category(long categoryId, String name, String picture,Category categoryByPatternCategory, List<Category> categoriesByCategoryId) {
//        this.categoryId = categoryId;
//        this.name = name;
//        this.picture = picture;
//        this.categoryId = categoryId;
//        this.categoriesByCategoryId = categoriesByCategoryId;
//        this.categoryByPatternCategory = categoryByPatternCategory;
//
//    }
}
