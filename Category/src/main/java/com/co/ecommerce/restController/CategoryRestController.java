package com.co.ecommerce.restController;

import com.amazonaws.services.iotdata.model.ConflictException;
import com.co.ecommerce.domain.CategoryDto;
import com.co.ecommerce.domain.ResponseErrorDto;
import com.co.ecommerce.manager.ICategoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.Consumes;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping(value = "/categories")
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class CategoryRestController implements ICategoryRestController {

    private final ICategoryManager iCategoryManager;

    @Autowired
    public CategoryRestController(ICategoryManager iCategoryManager) {
        this.iCategoryManager = iCategoryManager;
    }

    /**
     * Method used to create a category
     *
     * @param body category body request
     * @return response service
     */
    @PostMapping("")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseEntity create(@RequestBody CategoryDto body) {
        try {
            iCategoryManager.create(body);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex);
        }
    }

    /**
     * Method used to upload image to category
     *
     * @param categoryId category identifier
     * @param file       image to create and associates with the category
     * @return category object
     */
    @PatchMapping("/{categoryId}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseEntity createCategoryImage(@PathVariable(value = "categoryId") long categoryId,
                                              @RequestParam(value = "file") MultipartFile file) {
        try {
            iCategoryManager.uploadImage(file, categoryId);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex);
        }
    }

    /**
     * Method use to find category by id
     *
     * @param categoryId category identifier
     * @return category object
     */
    @Override
    @GetMapping("/{categoryId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity findCategoryById(@PathVariable(value = "categoryId") long categoryId) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(iCategoryManager.findCategoryById(categoryId));
        } catch (NotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(ex);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex);
        }
    }

    /**
     * Find categories paginate
     *
     * @param page        page number
     * @param pageSize    page size number
     * @param isLastChild validate if the response are only last child recursive entity
     * @return category list
     */
    @Override
    @GetMapping("")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity find(@QueryParam("page") Integer page, @QueryParam("pageSize") Integer pageSize, @QueryParam("isLastChild") boolean isLastChild) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(iCategoryManager.find(page, pageSize, isLastChild));
        } catch (ConflictException ex) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseErrorDto(ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex);
        }
    }

}