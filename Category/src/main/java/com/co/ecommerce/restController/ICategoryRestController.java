package com.co.ecommerce.restController;

import com.co.ecommerce.domain.CategoryDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

public interface ICategoryRestController {
    ResponseEntity create(CategoryDto body);

    ResponseEntity createCategoryImage(long categoryId, MultipartFile file);

    ResponseEntity findCategoryById(long categoryId);

    ResponseEntity find(Integer page, Integer sizePage, boolean isChild);
}
