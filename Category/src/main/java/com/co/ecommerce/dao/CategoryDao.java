package com.co.ecommerce.dao;

import com.co.ecommerce.model.Category;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.ws.rs.NotFoundException;
import java.util.List;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {SystemException.class})
public class CategoryDao implements ICategoryDao {

    @PersistenceContext
    private EntityManager em;

    /**
     * Method used to create a category
     *
     * @param category category database entity to create
     */
    @Override
    public void create(Category category) {
        em.persist(category);
        em.flush();
    }

    /**
     * Method used to update a category
     *
     * @param category category database entity to update
     */
    public void update(Category category) {
        em.merge(category);
        em.flush();
    }


    /**
     * @param id category id
     * @return category entity database
     * @throws NotFoundException Validate if the category doesn't exist
     */
    public Category findCategoryById(long id) throws NotFoundException {
        try {
            return em.createNamedQuery("Category.findById", Category.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (NoResultException ex) {
            throw new NotFoundException("Not found");
        }
    }

    /**
     * Method use to find categories
     *
     * @return list to category database entity
     */
    public List<Category> find() {
        try {
            return em.createNamedQuery("Category.findAll", Category.class)
                    .getResultList();
        } catch (NoResultException ex) {
            throw new NotFoundException("Not found");
        }
    }

}
