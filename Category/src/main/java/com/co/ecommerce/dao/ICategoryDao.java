package com.co.ecommerce.dao;

import com.co.ecommerce.model.Category;

import javax.ws.rs.NotFoundException;
import java.util.List;

public interface ICategoryDao {

    void create(Category category);

    void update(Category category);

    Category findCategoryById(long id) throws NotFoundException;

    List<Category> find();

}
