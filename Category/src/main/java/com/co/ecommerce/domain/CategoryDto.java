package com.co.ecommerce.domain;

import lombok.*;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CategoryDto {
    private long categoryId;
    private String name;
    private String picture;
}

