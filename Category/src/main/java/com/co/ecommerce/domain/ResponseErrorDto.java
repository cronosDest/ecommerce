package com.co.ecommerce.domain;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ResponseErrorDto implements Serializable {

    private String message;

}
