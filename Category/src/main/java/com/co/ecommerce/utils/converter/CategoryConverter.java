package com.co.ecommerce.utils.converter;

import com.co.ecommerce.domain.CategoryDto;
import com.co.ecommerce.model.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryConverter {

    public Category convertCategoryDtoToCategory(CategoryDto categoryDto){
        return Category.builder()
                .name(categoryDto.getName())
                .picture(categoryDto.getPicture())
                .build();
    }

    public CategoryDto convertCategoryToCategoryDto(Category category){
        return CategoryDto.builder()
                .categoryId(category.getCategoryId())
                .name(category.getName())
                .picture(category.getPicture())
                .build();
    }

}
