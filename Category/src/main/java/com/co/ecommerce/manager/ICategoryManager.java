package com.co.ecommerce.manager;

import com.co.ecommerce.domain.CategoryDto;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.NotFoundException;
import java.io.IOException;
import java.util.List;

public interface ICategoryManager {

    void create(CategoryDto categoryDto);

    void uploadImage( MultipartFile file,long categoryId) throws NotFoundException, IOException;

    CategoryDto findCategoryById(long categoryId);

    List<CategoryDto> find(Integer page, Integer sizePage, boolean idChild);

}
