package com.co.ecommerce.manager;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.iotdata.model.ConflictException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.co.ecommerce.domain.CategoryDto;
import com.co.ecommerce.model.Category;
import com.co.ecommerce.utils.constant.EnvironmentVariables;
import com.co.ecommerce.utils.converter.CategoryConverter;
import com.co.ecommerce.dao.ICategoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class CategoryManager implements ICategoryManager {

    private final ICategoryDao iCategoryDao;
    private final CategoryConverter categoryConverter;
    public EnvironmentVariables environmentVariables;
    AmazonS3 s3client;

    @Autowired
    public CategoryManager(ICategoryDao iCategoryDao, CategoryConverter categoryConverter,
                           EnvironmentVariables environmentVariables) {
        this.iCategoryDao = iCategoryDao;
        this.categoryConverter = categoryConverter;
        this.environmentVariables = environmentVariables;
        //Set AWS credentials
        AWSCredentials credentials = new BasicAWSCredentials(
                environmentVariables.ACCESS_KEY,
                environmentVariables.SECRET_KEY
        );
        //Create AWS S3 client
        s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_EAST_2)
                .build();
    }

    /**
     * Method used to create a category
     *
     * @param categoryDto category object
     */
    @Override
    public void create(CategoryDto categoryDto) {
        iCategoryDao.create(categoryConverter.convertCategoryDtoToCategory(categoryDto));
    }

    /**
     * Method used set new image to the category
     *
     * @param multipartFile new file
     * @param categoryId    category identifier
     * @throws IOException Input/Output Exception
     */
    @Override
    public void uploadImage(MultipartFile multipartFile, long categoryId) throws IOException {
        Category category = iCategoryDao.findCategoryById(categoryId);
        String url = uploadS3Image(multipartFile);
        category.setPicture(url);
        iCategoryDao.update(category);
    }

    @Override
    public CategoryDto findCategoryById(long categoryId) {
        return categoryConverter.convertCategoryToCategoryDto(iCategoryDao.findCategoryById(categoryId));
    }


    /**
     * Method used to find categories
     *
     * @param page     page number
     * @param sizePage size page number
     * @param isChild  validate if the response are only the last child categories
     * @return category list
     */
    @Override
    public List<CategoryDto> find(Integer page, Integer sizePage, boolean isChild) {
        if (page == null || sizePage == null) {
            throw new ConflictException("The page and the page size can't be empty");
        }
        List<CategoryDto> categoryDtoList = new ArrayList<>();
        for (Category category : iCategoryDao.find()) {
            if (!isChild || category.getCategoriesByCategoryId().isEmpty()) {
                categoryDtoList.add(categoryConverter.convertCategoryToCategoryDto(category));
            }
        }
        return paginate(page, sizePage, categoryDtoList);
    }

    /**
     * Method used to paginate the response
     *
     * @param page            page number
     * @param sizePage        page size number
     * @param categoryDtoList category list filtered
     * @return
     */
    private List<CategoryDto> paginate(Integer page, Integer sizePage, List<CategoryDto> categoryDtoList) {
        List<CategoryDto> categoryDtos = new ArrayList<>();
        for (int i = 0; i < sizePage; i++) {
            int index = ((page - 1) * sizePage) + i;
            if ((index + 1) <= categoryDtoList.size()) {
                categoryDtos.add(categoryDtoList.get(index));
            }
        }
        return categoryDtos;
    }


    /**
     * Method use to send the image to S3 Service
     *
     * @param multipartFile new image
     * @return image url location
     * @throws IOException Input/Output Exception
     */
    private String uploadS3Image(MultipartFile multipartFile) throws IOException {
        String fileName = new Date().getTime() + multipartFile.getOriginalFilename();
        File file = convertMultiPartToFile(multipartFile);
        String url = uploadFileTos3bucket(fileName, file);
        file.delete();
        return url;
    }


    /**
     * Method use to convert multipart file to file
     *
     * @param file new image
     * @return multipart convert to file
     * @throws IOException Input/Output Exception
     */
    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convertFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
        FileOutputStream fos = new FileOutputStream(convertFile);
        fos.write(file.getBytes());
        fos.close();
        return convertFile;
    }

    /**
     * Consume S3 client
     *
     * @param fileName file name to send
     * @param file     file to send
     * @return url image location
     */
    private String uploadFileTos3bucket(String fileName, File file) {
        s3client.putObject(
                new PutObjectRequest(environmentVariables.BUCKET, fileName, file)
                        .withCannedAcl(CannedAccessControlList.PublicRead));
        URL url = s3client.getUrl(environmentVariables.BUCKET, fileName);
        return url.toString();
    }

}
